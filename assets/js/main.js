var dp = jQuery;
dp.noConflict();
dp(document).ready(function() {
    dp('.flexslider').flexslider({
        animation: "slide"
    });
    dp('a.scrollto').on('click', function(e){
        if(this.hash != "")
        {
            dp('html,body').scrollTo(this.hash, this.hash, {gap:{y:0}});
        }
        else
        {
            window.location = this.href;
        }
        e.preventDefault();

        if (dp('.navbar-collapse').hasClass('in')){
            dp('.navbar-collapse').removeClass('in').addClass('collapse');
        }
    });
    //RESPONSIVE VIDEO
    if(dp.fn.fitVids){
        dp('.fitvids').fitVids();
    }
    //BIG SLIDE
    dp('#home-slide').superslides({
        slide_easing: 'easeInOutCubic',
        slide_speed: 800,
        animation: 'fade', // You can choose either fade or slide
        play: 6000,
        pagination: true,
        navigation: false
    });
    //SERVICES SLIDER
    dp("#slider-services").sudoSlider({
        speed: 650,
        auto: true,
        pause: 3000,
        prevNext: false,
        responsive: true,
        useCSS: true,
        continuous: true,
        effect: "slide",
        updateBefore: true
    });
    //PORTFOLIO
    dp('.portfolioContainer').mixitup({
        filterSelector: '.portfolioFilter a',
        targetSelector: '.portfolio-item',
        effects: ['fade', 'scale']
    });
    //QUOTE SLIDE
    dp("#quote-slider").sudoSlider({
        customLink: 'a.quoteLink',
        speed: 425,
        prevNext: true,
        responsive: true,
        prevHtml: '<a href="#" class="quote-left-indicator"><i class="fa fa-angle-left"></i></a>',
        nextHtml: '<a href="#" class="quote-right-indicator"><i class="fa fa-angle-right"></i></a>',
        useCSS: true,
        continuous: true,
        effect: "fadeOutIn",
        updateBefore: true
    });

    //  Responsive layout, resizing the items   
    if (dp('.client-slider').length > 0) {
        dp('.client-slider').carouFredSel({
            responsive: true,
            width: '100%',
            scroll: 1,
            items: {
                width: 116,
                //  height: '30%',  //  optionally resize item-height
                visible: {
                    min: 1,
                    max: 5
                }
            }
        });
    }

    //BACK TO TOP
    dp("#backtotop").backToTop();
    //CALL TO ACTION
    /*
    var bg_img = dp(".call-ta").attr('data-background');
    dp(".call-ta").backstretch(bg_img);
    */
    //JQUERY Mobile Devices Responsive
    dp('body').mobileResponsive();

    //MAP
    var lats = dp("#map").attr('data-lat');
    var lngs = dp("#map").attr('data-lng');
    var data_address = dp("#map").attr('data-address');
    var color = dp("#map").attr('data-color');
    var saturation = 100;
    dp("#map").gmap3({
        map: {
            options: {
                center: [lats, lngs],
                zoom: 15,
                navigationControl: 0,
                scrollwheel: true,
                zoomControl: true,
                disableDefaultUI: true,
                streetViewControl: 0,
                draggable: true,
            }
        },
        marker: {
            values: [{
                latLng: [lats, lngs],
                data: data_address,
                options: {
                    icon: "images/map-marker.png"
                }
            }],
            options: {
                draggable: false
            },
            events: {
                click: function(marker, event, context) {
                    var map = dp(this).gmap3("get"),
                              infowindow = dp(this).gmap3({
                            get: {
                                name: "infowindow"
                            }
                        });
                    if (infowindow) {
                        infowindow.open(map, marker);
                        infowindow.setContent(context.data);
                    } else {
                        dp(this).gmap3({
                            infowindow: {
                                anchor: marker,
                                options: {
                                    content: context.data
                                }
                            }
                        });
                    }
                }

            }
        }
    });
    //Bootstrap Tooltip
    dp('a[data-toggle="tooltip"]').tooltip();
    //NIVOLightbox
    if(dp.fn.nivoLightbox){
        dp('.popup').nivoLightbox({
            effect: 'fall',
            beforeShowLightbox: function(){
                dp('#main-wrapper').addClass('blur');
            },  
            afterHideLightbox: function(lightbox){
                dp('#main-wrapper').removeClass('blur');
            }
        });
    }

    dp('#side-menu').metisMenu();
    dp(window).bind("load resize", function() {
        console.log(dp(this).width())
        if (dp(this).width() < 768) {
            dp('div.sidebar-collapse').addClass('collapse')
        } else {
            dp('div.sidebar-collapse').removeClass('collapse')
        }
    })
});
dp(window).load(function() {
    dp('#loader').fadeOut(500, "linear");
});

