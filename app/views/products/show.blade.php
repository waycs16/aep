@extends('layouts.application')

@section('title')
{{$product->title}}
@stop

@section('style')
{{$product->style}}
<style>
.compact-search{
    list-style-type:none;
    margin:0;
    padding:0;
    z-index: 1000;
    width: 100%;
    background-color: #eceff1;
}
.compact-search li{
    float:left;
    margin:0;
    padding:10px 27px;
}
.compact-search li a{
    float:left;
    text-indent:-99999px;
    height:122px;
    width:122px;
    background-size: contain;
}
.compact-search li a.compact-2-3-m{
    background:url(/assets/images/compact-house.png) no-repeat 0 0;
    background-size: contain;

}
.compact-search li a.compact-2-3-m:hover, .compact-search li a.compact-2-3-m-active{
    background:url(/assets/images/compact-house-hover.png) no-repeat 0 0;;
    background-size: contain;
}
.compact-search li a.compact-3-4-m{
    background:url(/assets/images/modern-house.png) no-repeat 0 0;
    background-size: contain;
}
.compact-search li a.compact-3-4-m:hover, .compact-search li a.compact-3-4-m-active{
    background:url(/assets/images/modern-house-hover.png) no-repeat 0 0;;
    background-size: contain;
}
.compact-search li a.compact-4-5-m{
    background:url(/assets/images/premium-house.png) no-repeat 0 0;
    background-size: contain;
}
.compact-search li a.compact-4-5-m:hover, .compact-search li a.compact-4-5-m-active{
    background:url(/assets/images/premium-house-hover.png) no-repeat 0 0;;
    background-size: contain;
}
.compact-search li a.compact-3-floor{
    background:url(/assets/images/luxury-house.png) no-repeat 0 0;
    background-size: contain;
}
.compact-search li a.compact-3-floor:hover, .compact-search li a.compact-3-floor-active{
    background:url(/assets/images/luxury-house-hover.png) no-repeat 0 0;
    background-size: contain;
}
.compact-search li:first-child{
    padding:29px 15px 0 24px;
}

.menu-padding {
    padding-top:40px;
}
.content p {
    margin-bottom:20px;
}
.sticky {
    position:fixed;
    top:0;
}
</style>
@stop

@section('main')

<!-- BEGIN BLOG -->
            <section id="blog" class="blog">
                <div class="row">
                    <div class="col-md-9">
                        <div id="primary" class="row">
                            <div class="col-md-12">
                                <!-- BEGIN ARTICLE -->
                                <article class="post">
                                    <div class="post-thumb">
                                        <a class="popup f-link" href="{{ url($product->cover->url('original')) }}" data-lightbox-gallery="gallery1"> 
                                            <img src="{{ url($product->cover->url('large')) }}" alt="{{ $product->title }}" />
                                        </a>
                                    </div>
                                    <div class="post-title">
                                        <h1><a href="{{ url(action('ProductsController@show', $product->slug)) }}">{{ $product->title }}</a>
                                        </h1>
                                    </div>
                                    <div class="post-meta">
                                        <span class="dates">{{ date("d F Y",strtotime($product->created_at)) }}</span>
                                    </div>
                                    <div class="post-content">
                                        <p>{{ $product->content }}
                                        </p>

                                    </div>
                                </article>
                                <!-- END ARTICLE -->
                            </div>
                            
                        </div>

                        <div id="related" class="row blog-front">
                            <h3 class="widget-title thai-regular">
                                แบบบ้านล่าสุด
                            </h3>
                            <div class="col-md-12">
                                <?php $products = Product::where('post_type', '=', 'product')->orderBy('id', 'desc')->paginate(3);?>
                                @foreach ($products as $related_product)
                                <article class="col-md-4 col-sm-6">
                                    <figure class="blog-thumb"> <span class="f-img-wrap"><img src="{{ url($related_product->cover->url('large')) }}" alt="{{ $related_product->title }}"></span> 
                                    </figure>
                                    <div class="post-area">
                                        <a href="{{ url(action('PostsController@show', $related_product->slug)) }}" class="f-link"></a>
                                        <h4><a href="{{ url(action('PostsController@show', $related_product->slug)) }}" class="f-link"></a><a class="btn btn-light small f-link" href="{{ url(action('ProductsController@show', $related_product->slug)) }}">{{ $related_product->code() }}</a></h4> 
                                    </div>
                                </article>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 widgetbar">
                        <div class="categories-widget">
                            <h3 class="widget-title thai-regular">
                                แบบบ้าน รุ่น {{ $product->code() }}
                            </h3>
                            <ul>      
                                <li>ราคา {{ $product->price() }} บาท/ตร.ม.</li>   
                                <li>
                                    พื้นที่ใช้สอย   {{ $product->living_area() }}     ตารางเมตร
                                </li>                      
                                <li>
                                    ห้องนอน / ห้องน้ำ: {{ $product->room() }}
                                </li>
                                <li>
                                    ห้องสำหรับครอบครัว / ที่จอดรถ: {{ $product->room_other()}}
                                </li>
                                <li>
                                    พื้นที่ใช้สอย   {{ $product->living_area() }}     ตารางเมตร
                                </li>
                                <li><a title="วัสดุมาตรฐาน" href="{{ $product->material_standard() }}" target="_blank">วัสดุมาตรฐาน</a></li>
                            </ul>
                        </div>
                        <!-- /widget -->
                        <div class="categories-widget">
                            <h3 class="widget-title thai-regular">
                                แบบแปลนบ้าน
                            </h3>
                            @foreach($product->images as $image)
                            <a class="popup f-link" href="{{ url($image->image->url('original')) }}" title="{{ $product->title }}" data-lightbox-gallery="gallery1"> 
                                <span class="f-img-wrap">
                                    <img src="{{ url($image->image->url('large')) }}" alt="{{ $product->title }}" style="max-width:100%; padding: 5px;">
                                </span> 
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BLOG -->


@stop
