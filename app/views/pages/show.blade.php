@extends('layouts.application')

@section('title')
{{$page->title}}
@stop

@section('style')
<style>
{{$page->style}}
</style>
@stop

@section('main')
            
<!-- BEGIN HOME -->
@if($page->images->count() > 0)
    
<section id="home" class="home pageslider">
    <div id="home-slide">
        <ul class="slides-container text-center">
            @foreach($page->images as $image)
            <li>
                <div class="slide-text">
                    {{ $image->caption }}
                    <br>
                </div>
                <div class="slide-filter">&nbsp;</div>
                <div style="background-image: url('{{ url($image->image->url('original'))}}');" class="bg-parallax parallaxize">&nbsp;</div>
            </li>
            @endforeach
        </ul>
        <nav class="slides-navigation slidez">
            <a href="javascript:;" class="next f-link"> <i class="fa fa-angle-right"></i> 
            </a>
            <a href="javascript:;" class="prev f-link"> <i class="fa fa-angle-left"></i> 
            </a>
        </nav>
    </div>
</section>
@endif
<!-- END HOME -->

{{$page->content}}

@stop


@section('script')
<script type="text/javascript">
(function() {
 	var $ = jQuery;
	$.noConflict();
	
	$(document).ready(function(){
		$('.step-flexslider').flexslider({
	        animation: "slide",
	        slideshow: false,
	        controlNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
	        directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
	        prevText: "ก่อนหน้า",           //String: Set the text for the "previous" directionNav item
	        nextText: "ถัดไป",
	        after: function(slider){
	        	currentSlide = slider.currentSlide;
	        	$('.nav-tabs li').removeClass('active');
						if(currentSlide == 7)
						{
							$('#btn-payment').parent().addClass('active');
						}
						else if(currentSlide == 8)
						{
							$('#btn-warranty').parent().addClass('active');
						}
						else
						{
							$('#btn-step').parent().addClass('active');
						}
	        }
	    });
	});

	$('#btn-step').on('click', function(){
		$('.step-flexslider').data('flexslider').flexAnimate(0);
		$('.nav-tabs li').removeClass('active');
		$(this).parent().addClass('active');
	});
	$('#btn-payment').on('click', function(){
		$('.step-flexslider').data('flexslider').flexAnimate(7);
		$('.nav-tabs li').removeClass('active');
		$(this).parent().addClass('active');
	});
	$('#btn-warranty').on('click', function(){
		$('.step-flexslider').data('flexslider').flexAnimate(8);
		$('.nav-tabs li').removeClass('active');
		$(this).parent().addClass('active');
	});
 
	$('#prev a').on('click', function(){
		$('.step-flexslider').flexslider("prev");
	});
 
	$('#next a').on('click', function(){
		$('.step-flexslider').flexslider("next");
	});
}());
</script>
@stop