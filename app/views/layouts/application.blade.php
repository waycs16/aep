<!DOCTYPE html>
<html lang="en">

<?php echo View::make('layouts.head') ?>      

<body data-spy="scroll">
    <div id="loader"></div>
    <div id="main-wrapper">
        @if(isset($page))
        <?php echo View::make('layouts.sidebar', compact('page')) ?>      
        @else
        <?php echo View::make('layouts.sidebar') ?>      
        @endif
        <div id="container">
            @yield('main')
            
            <?php echo View::make('layouts.footer') ?>   
        </div>
    </div>

    <?php echo View::make('layouts.script') ?>  
</body>

</html>
