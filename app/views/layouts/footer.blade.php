<!-- BEGIN FOOTER -->
            <footer>
                <div class="row">
                    <div class="col-md-4">
                        <p>
                            <strong>Advance Enpower</strong>&nbsp; ©Copyright 2014 / All Rights Reserved : Created by <a href="http://www.expstudio.net">EXP Studio</a>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p>สำนักงาน : 
                            <br>120/18 หมู่ 17 ซ.โพธิ์ทอง ถ.เหล่านาดี 
                            <br>ต.ในเมือง อ.เมืองขอนแก่น จ.ขอนแก่น 40000
                            <br><table border="0px" cellpadding="5px" cellspacing="0px">
                            <tr>
                                <td>โทรศัพท์ </td>
                                <td><a href="tel:043 468 774" alt="Call to 043 468 774">043-468-774</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:08-6310-0256" alt="Call to 08-6310-0256">086-310-0256</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:083-788-2656">083-788-2656</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:086-011-3005">086-011-3005</a></td>
                            </tr>
                            <tr>
                                <td>โทรสาร </td>
                                <td><a href="tel:043-468-774">043-468-774</a></td>
                            </tr>
                            </table>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p>                          
                            Office : 
                            <br>120/18, Moo 17, Soi Phothong, Raonadee Rd., 
                            <br>Nai Muang, Muang Khon Kaen, Khonkaen, 40000
                            <br><table border="0px" cellpadding="5px" cellspacing="0px">
                            <tr>
                                <td>Telephone </td>
                                <td><a href="tel:043 468 774" alt="Call to 043 468 774">043-468-774</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:08-6310-0256" alt="Call to 08-6310-0256">086-310-0256</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:083-788-2656">083-788-2656</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:086-011-3005">086-011-3005</a></td>
                            </tr>
                            <tr>
                                <td>Fax </td>
                                <td><a href="tel:043-468-774">043-468-774</a></td>
                            </tr>
                            </table>
                        </p>
                    </div>
                </div>
            </footer>
            <!-- END FOOTER -->


    <div class="totop" id="backtotop">
        <span>
            <a href="#home" class="first sscroll"><i class="fa fa-angle-up"></i></a>
            <a href="#home" class="hover sscroll"><i class="fa fa-angle-up"></i></a>
        </span>
    </div>