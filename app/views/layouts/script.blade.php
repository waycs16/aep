<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- Jquery Source -->
    {{ HTML::script('assets/js/libs/jquery-1.11.0.min.js') }}
    <!-- Nivo Light Box Source -->
    {{ HTML::script('assets/js/libs/nivo-lightbox.min.js') }}
    <!-- Bootstrap Source -->
    {{ HTML::script('assets/js/libs/bootstrap.min.js') }}
    {{ HTML::script('assets/js/libs/bootstrap-hover-dropdown.min.js') }}
    <!-- Superslides Source -->
    {{ HTML::script('assets/js/libs/jquery.superslides.min.js') }}
    <!-- Jquery Scrollto Source -->
    {{ HTML::script('assets/js/libs/jquery.scrollto.js') }}
    <!-- Sudo Slider Source -->
    {{ HTML::script('assets/js/libs/jquery.sudoslider.min.js') }}
    <!-- flex slider Source -->
    {{ HTML::script('assets/js/jquery.flexslider.min.js') }}
    <!-- Mixitup Source -->
    {{ HTML::script('assets/js/libs/jquery.mixitup.min.js') }}
    <!-- Back to Top Source -->
    {{ HTML::script('assets/js/libs/jquery.backtotop.js') }}
    <!-- Backstretch Source -->
    {{ HTML::script('assets/js/libs/jquery.backstretch.min.js') }}
    <!-- CarouFredSel Source -->
    {{ HTML::script('assets/js/libs/jquery.carouFredSel-6.2.1-packed.js') }}
    <!-- Mobile Responsive Javascript -->
    {{ HTML::script('assets/js/libs/jquery.mobileresponsive.js') }}
    <!-- Google Map Source -->
    {{ HTML::script('assets/js/libs/gmap3.min.js') }}
    <script src='https://maps.googleapis.com/maps/api/js?sensor=true'></script>
    {{ HTML::script("assets/js/plugins/metisMenu/jquery.metisMenu.js") }}
    <!-- Main Action Javascript -->
    {{ HTML::script('assets/js/main.js') }}
    {{ Setting::SITE_SCRIPT() }}
    @yield('script')