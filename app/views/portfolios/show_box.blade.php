@extends('layouts.application_box')

@section('style')
    {{ HTML::style('assets/css/libs/owl-carousel/owl.carousel.css') }}
    {{ HTML::style('assets/css/libs/owl-carousel/owl.transitions.css') }}
<style type="text/css">
body {
    background-color: rgba(255, 255, 255, 0.86);
    padding: 10px;
}

.post .post-title {
    right: auto;
}
.row {
    margin-right: auto;
}

.post .post-thumb {
    position: relative;
    text-align: center;
}

.post .post-thumb img {
    width: 95%;
}

.post .post-meta {
    margin-bottom: 10px;
}

#primary {
    margin-bottom: 0px;
}
.post .post-meta span {
    font-size: 17px;
}
#sync1 .item{
    padding: 0px 0px;   
    color: #FFF;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    text-align: center;
}
#sync2 .item{
    padding: 0px 0px;
    margin: 5px;
    color: #FFF;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    text-align: center;
    cursor: pointer;
}
#sync2 .item h1{
  font-size: 18px;
}
#sync2 .synced .item{
  background: #0c83e7;
  padding: 3px;
}
.post .post-thumb img {
    width: 100%;
    margin-bottom: 0;
}
</style>
@stop

@section('main')

<!-- BEGIN BLOG -->
            <section id="blog" class="blog">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <!-- BEGIN ARTICLE -->
                            <article class="post">
                                <div id="primary" class="row col-sm-8">
                                    <div class="post-thumb">
                                        @if($portfolio->images()->count() > 0)
                                        <div id="sync1" class="owl-carousel">
                                            <div class="item"><img src="{{ url($portfolio->cover->url('large')) }}" alt="{{ $portfolio->title }}" /></div>
                                            @foreach($portfolio->images as $image)
                                            <div class="item"><img src="{{url($image->image->url('original'))}}" alt=""></div>
                                            @endforeach
                                        </div>
                                        <div id="sync2" class="owl-carousel">
                                            <div class="item"><img src="{{ url($portfolio->cover->url('large')) }}" alt="{{ $portfolio->title }}" /></div>
                                            @foreach($portfolio->images as $image)
                                            <div class="item"><img src="{{url($image->image->url('original'))}}" alt=""></div>
                                            @endforeach
                                        </div>
                                        @else
                                        <a href="{{ url(action('PortfoliosController@show', $portfolio->slug)) }}">
                                            <img src="{{ url($portfolio->cover->url('large')) }}" alt="{{ $portfolio->title }}" />
                                        </a>
                                        @endif
                                    </div>
                                    <div class="post-title">
                                        <h1><a href="{{ url(action('PortfoliosController@show', $portfolio->slug)) }}">{{ $portfolio->title }}</a>
                                        </h1>
                                    </div>
                            
                                </div>
                                <div class="post-content col-sm-3">
                                    {{ $portfolio->content }}   
                                    <div class="post-meta">
                                        <span class="dates">{{ date("d F Y",strtotime($portfolio->created_at)) }}</span>
                                    </div>                                 
                                </div>
                            </article>
                                
                            <!-- END ARTICLE -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BLOG -->


@stop


@section('script')
<script type="text/javascript">
var $ = jQuery;
$(document).ready(function() {
  var sync1 = $("#sync1");
  var sync2 = $("#sync2");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    pagination:false,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 6,
    itemsDesktop      : [1199,6],
    itemsDesktopSmall     : [979,6],
    itemsTablet       : [768,5],
    itemsMobile       : [479,4],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }
});
</script>
@stop