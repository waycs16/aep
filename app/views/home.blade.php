@extends('layouts.application')
@section('title')
ยินดีต้อนรับสู่
@stop

@section('style')
    <!-- PACE Loader -->
    {{ HTML::script('assets/js/libs/pace.min.js') }}
    {{ HTML::style('assets/css/libs/pace.css') }}
    {{ HTML::style('assets/css/scheme/blue.css') }}
    <!-- END PACE Loader -->
@stop
@section('main')
            
            <!-- BEGIN HOME -->
            <?php $home_page = Page::where('slug', '=', 'home')->first(); ?>
            @if($home_page && $home_page->images->count() > 0)
                
            <section id="home" class="home">
                <div id="home-slide">
                    <ul class="slides-container text-center">
                        @foreach($home_page->images as $image)
                        <li>
                            <div class="slide-text">
                                {{ $image->caption }}
                                <br>
                            </div>
                            <div class="slide-filter">&nbsp;</div>
                            <div style="background-image: url('{{ url($image->image->url('original'))}}');" class="bg-parallax parallaxize">&nbsp;</div>
                        </li>
                        @endforeach
                    </ul>
                    <nav class="slides-navigation slidez">
                        <a href="javascript:;" class="next f-link"> <i class="fa fa-angle-right"></i> 
                        </a>
                        <a href="javascript:;" class="prev f-link"> <i class="fa fa-angle-left"></i> 
                        </a>
                    </nav>
                </div>
            </section>
            @endif
            <!-- END HOME -->
            <section id="house-plan" class="blog-front odd">
                <div class="row">
                    <div class="col-md-12 mg-bt-80">
                        <div class="header-content">
                            <h2 class="thai-regular"><span><h2>House Plan</h2>  
                            <h3>“แบบบ้าน ....&nbsp;<a href="#" class="f-link">บริษัท แอ็ดวานซ์ เอนพาวเวอร์ จำกัด</a>"</h3> 
                            </span>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php $products = Product::where('post_type', '=', 'product')->orderBy('id', 'asc')->paginate(6);?>
                    @foreach ($products as $product)
                    <article class="col-md-4 col-sm-6">
                        <figure class="blog-thumb"> <span class="f-img-wrap"><img src="{{ url($product->cover->url('large')) }}" alt="{{ $product->title }}"></span> 
                        </figure>
                        <div class="post-area">
                            <a href="{{ url(action('PostsController@show', $product->slug)) }}" class="f-link"></a>
                            <h4><a href="{{ url(action('PostsController@show', $product->slug)) }}" class="f-link"></a><a class="btn btn-light small f-link" href="{{ url(action('ProductsController@show', $product->slug)) }}">{{ $product->code() }}</a></h4> 
                        </div>
                    </article>
                    @endforeach
                </div>
            </section>
            <!-- END BLOG -->
            <!-- BEGIN ABOUT US -->
            <?php $about_page = Page::where('slug', '=', 'about-us')->first(); ?>
            @if($about_page)
                {{ $about_page->content }}
            @endif
            <?php $warranty_page = Page::where('slug', '=', 'warranty')->first(); ?>
            @if($warranty_page)
                {{ $warranty_page->content }}
            @endif
            <?php $team_page = Page::where('slug', '=', 'team')->first(); ?>
            @if($team_page)
                {{ $team_page->content }}
            @endif
            <?php $sponsor_page = Page::where('slug', '=', 'sponsor')->first(); ?>
            @if($sponsor_page)
                {{ $sponsor_page->content }}
            @endif
            <!-- END ABOUT US -->

            <!-- BEGIN PORTFOLIO -->
            <?php $portfolio_page = Page::where('slug', '=', 'portfolio')->first(); ?>
            @if($portfolio_page)
                <?php 
                    $content = $portfolio_page->content;
                    $portfolio_content = View::make('portfolios.portfolio')->render();
                    echo str_replace('[PORTFOLIO_PATH]', $portfolio_content, $content);
                ?>
            @endif 
            <!-- END PORTFOLIO -->
            <!-- BEGIN TESTIMONIAL -->
            <?php $testimonial = Page::where('slug', '=', 'testimonial')->first(); ?>
            @if($testimonial)
                {{ $testimonial->content }}
            @endif 
            <!-- END TESTIMONIAL -->
            <!-- BEGIN Example Section -->
            <section>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="social-icon">
                            <li><a href="javascript:;"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="javascript:;"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="javascript:;"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li><a href="javascript:;"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li><a href="javascript:;"><i class="fa fa-dribbble"></i></a>
                            </li>
                            <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <!-- END Example Section -->
            
            <!-- BEGIN CONTACT -->
            <section id="contact" class="contact odd">
                <div class="row">
                    <div class="col-md-12 mg-bt-80">
                        <div class="header-content">
                            <h2>ติดต่อเรา</h2>
                            <h3>ติดต่อสอบถาม... <a href="javascript:;">บริษัท แอ็ดวาซ์ เอ็นพาวเวอร์ จำกัด</a>.
                            </h3>
                        </div>                        
                    </div>
                    <div class="col-sm-6 col-md-8 col-xs-12">                        
                        <!-- BEGIN MAP -->
                        <section class="map" id="map" data-lat='16.414909' data-lng='102.814732' 
                        data-address="<strong>Advance Enpower Company</strong><br/><br/>KHON KAEN OFFICE : 
                                        <br>120/18, Moo 17, Soi Phothong, Raonadee Rd., 
                                        <br>Nai Muang, Muang Khon Kaen, Khonkaen, 40000<br><br>Google Map 16.414909, 102.814732<br><br>Tel & Fax : 66(43) 468 774<br><br>" data-color="#26292E">
                        </section>
                        <!-- END MAP -->
                    </div>
                    <div class="col-sm-6 col-md-4 col-xs-12">
                        <p><strong>สำนักงาน : </strong>
                            <br>120/18 หมู่ 17 ซ.โพธิ์ทอง ถ.เหล่านาดี 
                            <br>ต.ในเมือง อ.เมืองขอนแก่น จ.ขอนแก่น 40000
                        </p>
                        <br/>
                        <p>                          
                            <strong>Office : </strong>
                            <br>120/18, Moo 17, Soi Phothong, Laonadee Rd., 
                            <br>Nai Muang, Muang Khon Kaen, Khonkaen, 40000
                        </p>
                        <p>
                            <table border="0px" cellpadding="5px" cellspacing="0px">
                            <tr>
                                <td>โทรศัพท์ (Tel) </td>
                                <td><a href="tel:043 468 774" alt="Call to 043 468 774">043-468-774</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:08-6310-0256" alt="Call to 08-6310-0256">086-310-0256</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:083-788-2656">083-788-2656</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="tel:086-011-3005">086-011-3005</a></td>
                            </tr>
                            <tr>
                                <td>โทรสาร (Fax) </td>
                                <td><a href="tel:043-468-774">043-468-774</a></td>
                            </tr>
                            </table>
                        </p>
                    </div>
                </div>
            </section>
            <!-- END CONTACT -->
@stop            

@section('script')
<script type="text/javascript">
var dp = jQuery;
dp.noConflict();
dp(window).load(function() {
    dp('#loader').fadeOut(500, "linear");
});
</script>
@stop