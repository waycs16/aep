<?php
use Expstudio\LaraClip\LaraClip;

class Product extends Post {
  protected $table = 'posts';

  public static $metas = array(
      'price' => 'ช่วงราคา',
      'code' => 'Product Code',
      'living_area' => 'พื้นที่ใช้สอย',
      'floor' => 'จำนวนชั้น',
      'room' => 'ห้องนอน / ห้องน้ำ',
      'room_other' => 'ห้องนั่งเล่น / ที่จอดรถ',
      'material_standard' => 'วัสดุมาตรฐาน'
    );

  public function __construct(array $attributes = array()) {
      parent::__construct($attributes);
  }
  
  protected $attributes = array(
    'post_type' => 'product'
  );

  public function translation(){
    return $this->hasMany('ProductTranslation', 'page_id');
  }

  public function categories(){
    return $this->belongsToMany('ProductCategory', 'product_categories_posts', 'post_id', 'product_category_id')->withPivot('products');
  }

  public function code(){
    $code = $this->metas()->where('meta_key','=','code')->first();
    return $code ? $code->meta_value : '';
  }

  public function price(){
    $price = $this->metas()->where('meta_key','=','price')->first();
    return $price ? $price->meta_value : '';
  }

  public function living_area(){
    $living_area = $this->metas()->where('meta_key','=','living_area')->first();
    return $living_area ? $living_area->meta_value : '';
  }


  public function floor(){
    $floor = $this->metas()->where('meta_key','=','floor')->first();
    return $floor ? $floor->meta_value : '';
  }

  public function room(){
    $room = $this->metas()->where('meta_key','=','room')->first();
    return $room ? $room->meta_value : '';
  }

  public function room_other(){
    $room_other = $this->metas()->where('meta_key','=','room_other')->first();
    return $room_other ? $room_other->meta_value : '';
  }

  public function material_standard(){
    $material_standard = $this->metas()->where('meta_key','=','material_standard')->first();
    return $material_standard ? $material_standard->meta_value : '';
  }
}