<?php
$data = array(
    '_json' => array(
    	 array(
		    "email" => "john.doe@sendgrid.com",
		    "sg_event_id" => "VzcPxPv7SdWvUugt-xKymw",
		    "sg_message_id" => "142d9f3f351.7618.254f56.filter-147.22649.52A663508.0",
		    "timestamp" => 1386636112,
		    "smtp-id" => "<142d9f3f351.7618.254f56@sendgrid.com>",
		    "event" => "processed",
		    "category" => array(
		      "category1",
		      "category2",
		      "category3"
		    ),
		    "id" => "001",
		    "purchase" => "PO1452297845",
		    "uid" => "123456" 

    	)
    	,array(
	    	"uid" => "123456",
		    "status" => "5.1.1",
		    "sg_event_id" => "X_C_clhwSIi4EStEpol-SQ",
		    "reason" => "550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces. Learn more at http: \/\/support.google.com\/mail\/bin\/answer.py?answer=6596 do3si8775385pbc.262 - gsmtp ",
		    "newsletter_code" => "NL140529067365F5",
		    "event" => "bounce",
		    "email" => "asdfasdflksjfe@sendgrid.com",
		    "timestamp" => 1386637483,
		    "smtp-id" => "<142da08cd6e.5e4a.310b89@localhost.localdomain>",
		    "type" => "bounce",
		    "category" => array(
		      "category1",
		      "category2",
		      "category3"
		    ),
		    "id" => "001"
    	)
  	)
);

$json = json_encode($data);

$ch = curl_init('http://localhost:3000/send_grid_event_api');                                                                      
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($json))                                                                       
);                                                                                                                   
 
$result = curl_exec($ch);
?>